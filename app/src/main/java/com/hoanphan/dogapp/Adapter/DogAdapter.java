package com.hoanphan.dogapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hoanphan.dogapp.DetailFragment;
import com.hoanphan.dogapp.R;
import com.hoanphan.dogapp.model.DogBreed;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;

public class DogAdapter extends RecyclerView.Adapter<DogAdapter.ViewHolder> {
    Context context;
    ArrayList<DogBreed> dogs;

    public DogAdapter(Context context, ArrayList<DogBreed> dogs) {
        this.context = context;
        this.dogs = dogs;
    }

    @NonNull
    @Override
    public DogAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dog_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DogAdapter.ViewHolder holder, int i) {
        DogBreed dogBreed = dogs.get(i);
        holder.txtTenCho.setText(dogBreed.name);
        holder.txtDecriptions.setText(dogBreed.lifeSpan);
        Picasso.with(context).load(dogBreed.url).into(holder.imgAvatar);
    }

    @Override
    public int getItemCount() {
        return dogs.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView txtTenCho,txtDecriptions;
        ImageView imgAvatar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTenCho = itemView.findViewById(R.id.txtName);
            txtDecriptions = itemView.findViewById(R.id.txtDescription);
            imgAvatar = imgAvatar.findViewById(R.id.imgAvatar);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, DetailFragment.class);
                    intent.putExtra("idDogs", (Serializable) dogs.get(getPosition()));
                    context.startActivity(intent);
                }
            });
        }
    }
}
