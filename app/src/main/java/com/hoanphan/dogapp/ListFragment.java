package com.hoanphan.dogapp;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.hoanphan.dogapp.Adapter.DogAdapter;
import com.hoanphan.dogapp.model.DogApi;
import com.hoanphan.dogapp.model.DogBreed;
import com.hoanphan.dogapp.modelview.DogApiService;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ListFragment extends Fragment {
    RecyclerView recyclerViewDogs;
    DogAdapter dogAdapter;
    ImageView imgAvatar;
    View view;
    ArrayList<DogBreed> listDog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_list,container,false);
        recyclerViewDogs=view.findViewById(R.id.rvDogs);
        getData();

        return view;
    }

    private void getData() {
        dogAdapter = new DogAdapter(null,listDog);
        recyclerViewDogs.setAdapter(dogAdapter);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        recyclerViewDogs.setLayoutManager(layoutManager);
        // Khởi tạo OkHttpClient để lấy dữ liệu.
        OkHttpClient client = new OkHttpClient();

        // Tạo request lên server.
        Request request = new Request.Builder()
                .url("https://raw.githubusercontent.com")
                .build();

        // Thực thi request.
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("Error", "Network Error");
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {

            }
        });
    }
}